FROM alpine:latest

RUN mkdir -p /path/to/vulnerable/version/of/log4j/
RUN wget https://repo1.maven.org/maven2/org/apache/logging/log4j/log4j-core/2.14.0/log4j-core-2.14.0.jar -P /path/to/vulnerable/version/of/log4j/
